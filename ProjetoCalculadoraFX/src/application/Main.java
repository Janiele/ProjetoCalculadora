package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws IOException {
		
			 /**toda vez que abrir o fxml ele vai:
	         * ler o fxml e carrega o fxml com o parent root, com o nome especifico, "nomedoarquivo.fxml";
	         * coloca esse fxml em uma cena com scene (root);
	         * titulo da janela;
	         * coloca essa cena em uma janela com o setScene passando por parametro scene;
	         * abre a janela com stage.show();
	         * resizable para a calculadora n�o redimencionar;
	         */
	        Parent root = FXMLLoader.load(getClass().getResource("CalculadoraMain.fxml"));
	    
	        Scene scene = new Scene(root);
	        primaryStage.setTitle("Calculadora");
	        primaryStage.setScene(scene);
	        primaryStage.show(); 
	        primaryStage.setResizable(false); 
	        //scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
				
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
